

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/gi")
public class IngenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public IngenierosServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		HTML5.comienzo(out, "gestion de Ingenieros");
		out.println("<form method=\"get\" action=\"/Gestion_de_Empleados/gi\">");
		
		out.println("<p><label for\"nom\">Nombre</label>");
		out.println("<input type=\"text\" name=\"nom\" />");
		
		out.println("<p><label for\"nss\">nss</label>");
		out.println("<input type=\"text\" name=\"nss\" />");
		
		out.println("<p><label for\"sal\">Salario</label>");
		out.println("<input type=\"number\" name=\"sal\" />");
		
		
		out.println("<p><label for\"esp\">Especialidad</label>");
		out.println("<input type=\"text\" name=\"esp\" />");
		
		out.println("<p><label for\"dep\">Departamento</label>");
		out.println("<select name=\"dep\">");
		
		
		
		agregarDepartamentos(out);
		insertarEmpleado(out,request);
		out.println("</select>");

		out.println("<input type=\"submit\" name=\"env\" value=\"Enviar\">");
		
				out.println( "</form>");
		HTML5.fin(out);
		
	}

	private void agregarDepartamentos(PrintWriter out) {
		try {
			Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados"
					,"root","lacasitos");
		Statement sql = c.createStatement();
		ResultSet rs = sql.executeQuery("select nombre from departamentos;");
		while(rs.next()) {
			rs.getString("nombre");
			out.printf("<option>%s</option>",rs.getString("nombre"));
			
		}
		rs.close();
		c.close();
		} catch (SQLException e) {
			
		}
	}
	
	private void insertarEmpleado(PrintWriter out,HttpServletRequest request) {
		Connection cnx;
		try {
			cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados"
					,"root","lacasitos");
			
		String nombre = request.getParameter("nom");
		String nss = request.getParameter("nss");
		String salario = request.getParameter("sal");
		String depar = request.getParameter("dep");
		String espe = request.getParameter("esp");
		
		PreparedStatement sql = cnx.prepareStatement("insert into empleados values(?,?,?)");
		sql.setString(1, nss);
		sql.setString(2, nombre);
		sql.setString(3, salario);
		
		sql.executeUpdate();
		
		PreparedStatement sql1 = cnx.prepareStatement("insert into ingenieros values(?,?)");
		sql1.setString(1, nss);
		sql1.setString(2, depar);
		
		sql1.executeUpdate();
		
		PreparedStatement sql2 = cnx.prepareStatement("insert into especialidades values(?,?)");
		sql2.setString(1, nss);
		sql2.setString(2, espe);
		
		sql2.executeUpdate();
		
		
		
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
	}
}
